import React, { Component } from "react";
import { connect } from "react-redux";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";

import Auxiliary from "util/Auxiliary";
import UserProfile from "./UserProfile";
import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
class SidebarContent extends Component {

  getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };

  render() {
    const { themeType, navStyle, pathname } = this.props;
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split('/')[1];
    return (<Auxiliary>

      <SidebarLogo />
      <div className="gx-sidebar-content">
        <div className={`gx-sidebar-notifications ${this.getNoHeaderClass(navStyle)}`}>
          <UserProfile />
          <AppsNavigation />
        </div>
        <CustomScrollbars className="gx-layout-sider-scrollbar">
          <Menu
            defaultOpenKeys={[defaultOpenKeys]}
            selectedKeys={[selectedKeys]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            mode="inline">
            <MenuItemGroup key="main" className="gx-menu-group" title={<IntlMessages id="sidebar.main" />}></MenuItemGroup>
            <SubMenu key="super admin" className={this.getNavStyleSubMenuClass(navStyle)} title={
              <span>
                <i className="icon icon-all-contacts" />
                   Super Admin
              </span>}>
              <Menu.Item key="Dashboard">
                <Link to="/Dashboard">
                  <i className="icon icon-listing-dbrd" />
                     Dashboard
                    </Link>
              </Menu.Item>
              <Menu.Item key="CategoryMaster">
                <Link to="/CategoryMaster">
                  <i className="icon icon-listing-dbrd" />
                     Category Create Master
                    </Link>
              </Menu.Item>
              <Menu.Item key="UserMaster">
                <Link to="/UserMaster">
                  <i className="icon icon-crypto" />
                     User Master
                    </Link>
              </Menu.Item>
              <Menu.Item key="ProfessionalMaster">
                <Link to="/ProfessionalMaster">
                  <i className="icon icon-listing-dbrd" />
                     Professional Master
                    </Link>
              </Menu.Item>
              <Menu.Item key="OrganizationMaster">
                <Link to="/OrganizationMaster">
                  <i className="icon icon-listing-dbrd" />
                     Organizational Master
                    </Link>
              </Menu.Item>
              <Menu.Item key="ProfessionalProfileMaster">
                <Link to="/ProfessionalProfileMaster">
                  <i className="icon icon-listing-dbrd" />
                     Professional profile Master
                    </Link>
              </Menu.Item>
              <Menu.Item key="OrganizationalProfileMaster">
                <Link to="/OrganizationalProfileMaster">
                  <i className="icon icon-listing-dbrd" />
                     Organzational profile Master
                    </Link>
              </Menu.Item>
            </SubMenu>
            <SubMenu key="Admin" className={this.getNavStyleSubMenuClass(navStyle)} title={
              <span>
                <i className="icon icon-all-contacts" />
                   Admin
              </span>}>
              <Menu.Item key="BookingDetails">
                <Link to="/UpcomingBookingDetails"><i className="icon icon-widgets" />
                 UpcomingBookingDetails
                 </Link>
              </Menu.Item>
              <Menu.Item key="BookingHistory">
                <Link to="/BookingHistory"><i className="icon icon-widgets" />
                Booking History
                 </Link>
              </Menu.Item>
              <Menu.Item key="Review">
                <Link to="/Review"> <i className="icon icon-listing-dbrd" />
              Review
              </Link>
              </Menu.Item>
              <Menu.Item key="availability">
                <Link to="/availability"> <i className="icon icon-listing-dbrd" />
              Availability
              </Link>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </CustomScrollbars>
      </div>
    </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({ settings }) => {
  const { navStyle, themeType, locale, pathname } = settings;
  return { navStyle, themeType, locale, pathname }
};
export default connect(mapStateToProps)(SidebarContent);

