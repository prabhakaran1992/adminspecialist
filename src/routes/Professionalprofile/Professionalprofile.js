import React, { Component } from "react";
import { Button, Divider, Modal, Card, Icon, Form, Input } from "antd";

const FormItem = Form.Item;
const confirm = Modal.confirm;
const data = [{
    key: '1',
    Orgid: 1,
    professional: 'Insurance',
    organisation: 'acots',
    email: 'acots@gmail.com',
    location: 'chennai',
    mobile: '9874563210',
}, {
    key: '2',
    Orgid: 2,
    professional: 'Insurance',
    organisation: 'acots',
    email: 'acots@gmail.com',
    location: 'chennai',
    mobile: '9874563210',
}, {
    key: '3',
    Orgid: 3,
    professional: 'Insurance',
    organisation: 'acots',
    email: 'acots@gmail.com',
    location: 'chennai',
    mobile: '9874563210',
}, {
    key: '4',
    Orgid: 4,
    professional: 'Insurance',
    organisation: 'acots',
    email: 'acots@gmail.com',
    location: 'chennai',
    mobile: '9874563210',
}];

class ProfessionalProfile extends React.Component {
    state = {
        filteredInfo: null,
        sortedInfo: null,
        visible: false,
        open: false
    };



    render() {
        // const {getFieldDecorator} = this.props.form;
        let { sortedInfo, filteredInfo } = this.state;
        sortedInfo = sortedInfo || {};
        filteredInfo = filteredInfo || {};


        return (
            <div>
                <Card title="Professional Profile Master">
                    <div className="table-operations">
                        <Form onSubmit={this.handleSubmit} >
                            <FormItem
                                label="Category"
                                labelCol={{ xs: 24, sm: 8 }}
                                wrapperCol={{ xs: 24, sm: 16 }}
                            >
                                <Input />
                            </FormItem>
                            <FormItem
                                label="Email"
                                labelCol={{ xs: 24, sm: 8 }}
                                wrapperCol={{ xs: 24, sm: 16 }}
                            >
                                <Input />
                            </FormItem>
                            <FormItem
                                wrapperCol={{ xs: 24, sm: { span: 12, offset: 5 } }}
                            >
                                <Button type="primary" htmlType="submit"> 
                                    Submit
            </Button>
                            </FormItem>
                        </Form>
                    </div>
                </Card>

            </div>
        );
    }
}

export default ProfessionalProfile;
