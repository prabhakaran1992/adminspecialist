import React, { Component } from "react";
import { Button, Divider, Modal, Card, Table, Form, Input } from "antd";

const FormItem = Form.Item;
const confirm = Modal.confirm;
const data = [{
    key: '1',
    name: 'test',
    professional:'Insurance',
    organisation:'acots',
    email:'acots@gmail.com',
    location:'chennai',
    mobile:'9874563210',
}, {
    key: '2',
    name: 'test',
    professional:'Insurance',
    organisation:'acots',
    email:'acots@gmail.com',
    location:'chennai',
    mobile:'9874563210',
}, {
    key: '3',
    name: 'test',
    professional:'Insurance',
    organisation:'acots',
    email:'acots@gmail.com',
    location:'chennai',
    mobile:'9874563210',
}, {
    key: '4',
    name: 'test',
    professional:'Insurance',
    organisation:'acots',
    email:'acots@gmail.com',
    location:'chennai',
    mobile:'9874563210',
}];

class ProfessionalMaster extends React.Component {
    state = {
        filteredInfo: null,
        sortedInfo: null,
        visible: false,
        open: false
    };

    showDeleteConfirm = () => {
        confirm({
            title: 'Are you sure delete this task?',
            content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                console.log('OK');
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }
    opencategory = () => {
        this.setState({
            open: true,
        })
    };
    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
            open:false
        });
    };
    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
            open:false
        });
    };
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter);
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };
    clearFilters = () => {
        this.setState({ filteredInfo: null });
    };
    clearAll = () => {
        this.setState({
            filteredInfo: null,
            sortedInfo: null,
        });
    };
    setAgeSort = () => {
        this.setState({
            sortedInfo: {
                order: 'descend',
                columnKey: 'age',
            },
        });
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }
    render() {
        // const {getFieldDecorator} = this.props.form;
        let { sortedInfo, filteredInfo } = this.state;
        sortedInfo = sortedInfo || {};
        filteredInfo = filteredInfo || {};
        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: 'Professional',
                dataIndex: 'professional',
                key: 'professional',
            },
            {
                title: 'Organisation',
                dataIndex: 'organisation',
                key: 'organisation',
            },
            {
                title: 'Mobile',
                dataIndex: 'mobile',
                key: 'mobile',
            },
            {
                title: 'Email',
                dataIndex: 'email',
                key: 'email',
            },
            {
                title: 'Location',
                dataIndex: 'location',
                key: 'location',
            },
            {
                title: 'Action',
                key: '',
                render: () =>
                    <span>
                        <span style={{ cursor: 'pointer' }} onClick={this.showModal} className="icon icon-edit"></span>
                        <Divider type="vertical" />
                        <span style={{ cursor: 'pointer' }} onClick={this.showDeleteConfirm} className="icon icon-close-circle"></span>
                    </span>
            }];

        return (
            <div>
                <Card title="Professional Master">
                    <div className="table-operations">
                        {/* <Button onClick={this.setAgeSort}>Sort age</Button>
                        <Button onClick={this.clearFilters}>Clear filters</Button>
                        <Button onClick={this.clearAll}>Clear filters and sorters</Button> */}
                        <Button type="primary" onClick={this.opencategory}>Add Professional</Button>
                    </div>
                    <Table className="gx-table-responsive" columns={columns} dataSource={data} onChange={this.handleChange} />
                </Card>
                <Modal
                    title="Edit Professional Master"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <Form onSubmit={this.handleSubmit}>
                        <FormItem
                            label="Name"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Professional"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Organisation"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Mobile"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Email"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Location"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        {/* <FormItem
              wrapperCol={{ xs: 24, sm: { span: 12, offset: 5 } }}
            >
              <Button type="primary" htmlType="submit">
                Submit
            </Button>
            </FormItem> */}
                    </Form>
                </Modal>
                <Modal
                    title="Add Professional"
                    visible={this.state.open}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                     <Form onSubmit={this.handleSubmit}>
                     <FormItem
                            label="Name"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Professional"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Organisation"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Mobile"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Email"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Location"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        {/* <FormItem
              wrapperCol={{ xs: 24, sm: { span: 12, offset: 5 } }}
            >
              <Button type="primary" htmlType="submit">
                Submit
            </Button>
            </FormItem> */}
                    </Form>
                </Modal>
            </div>
        );
    }
}

export default ProfessionalMaster;
