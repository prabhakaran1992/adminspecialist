import React from "react";
import {Route, Switch} from "react-router-dom";

import asyncComponent from "util/asyncComponent";
import SocialApps from "./socialApps/index";

const App = ({match}) => (
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}sample`} component={asyncComponent(() => import('./SamplePage'))}/>
      <Route path={`${match.url}Dashboard`} component={asyncComponent(() => import('./main/dashboard/Listing/index'))}/>
      <Route path={`${match.url}list`} component={asyncComponent(() => import('./customViews/eCommerce/ProductList'))}/>
      <Route path={`${match.url}UserMaster`} component={asyncComponent(() => import('./Usermaster/usermaster'))}/>
      <Route path={`${match.url}CategoryMaster`} component={asyncComponent(() => import('./CategoryMaster/Categorymaster'))}/>
      <Route path={`${match.url}ProfessionalMaster`} component={asyncComponent(() => import('./Professionalmaster/ProfessionalMaster'))}/>
      <Route path={`${match.url}OrganizationMaster`} component={asyncComponent(() => import('./Organisation/OrganizationMaster'))}/>
      <Route path={`${match.url}ProfessionalProfileMaster`} component={asyncComponent(() => import('./Professionalprofile/Professionalprofile'))}/>
      <Route path={`${match.url}OrganizationalProfileMaster`} component={asyncComponent(() => import('./Organizationalprofile/Organizationalprofile'))}/>
      <Route path={`${match.url}BookingHistory`} component={asyncComponent(() => import('./Admin Dashboard/Admin/ExpandableRow'))}/>
      <Route path={`${match.url}UpcomingBookingDetails`} component={asyncComponent(() => import('./Admin Dashboard/Admin/ExpandableRow'))}/>
      <Route path={`${match.url}review`} component={asyncComponent(() => import('./Admin Dashboard/Admin/Review'))}/>
      <Route path={`${match.url}availability`} component={asyncComponent(() => import('./Admin Dashboard/Admin/availability'))}/>
      <Route path={`${match.url}social-apps`} component={SocialApps}/>
    </Switch>
  </div>
);

export default App;
// ./customViews/eCommerce/ProductList'))}/>