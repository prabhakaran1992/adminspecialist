import React from "react";
import {Col, Row} from "antd";
import TicketList from "components/dashboard/CRM/TicketList";
import TaskByStatus from "components/dashboard/CRM/TaskByStatus";
import { Input, Button } from 'antd';
// import './Sample.css';
import Avatar from './upload';
import IntlMessages from "util/IntlMessages";
const { TextArea } = Input;
const ReviewPage = () => {
  return (
    <div class="display"> 
      {/* <h1 className="title gx-mb-4"><IntlMessages id="Bike World"/></h1> */}
      {/* <div className="gx-d-flex justify-content-center">
        <h4>Get Ready to Ride...
        <img style={{width:'150px'}}src={require("assets/images/bikelogo.png")} alt=""/>
        </h4>
      </div> */}
      {/* <Row>
      <Col sm={16}>
              <TicketList/>
            </Col>
            <Col sm={8}>
              <TaskByStatus/>
            </Col>
            </Row> */}
            <div>
            <TextArea rows={6} style={{width: '50%'}} />
            </div>
            <div style={{margin:'20px'}}>
            <Avatar/>
            <Button style={{background:"#00BFFF",color:"#fff", fontStyle:"Bold"}}>Submit</Button>
           
            </div>
    </div>
  );
};
export default ReviewPage;