import React from 'react';
import ReactDOM from 'react-dom';
import { Calendar, Alert, Checkbox, } from 'antd';
import { Typography, Divider } from 'antd';
import moment from 'moment';
import { DatePicker } from 'antd';
import { Input, Button } from 'antd';
// import Crud from '../UpcommingBooking/index.js';
import locale from 'antd/es/date-picker/locale/zh_CN';
import { Table } from 'antd';
class SamplePage extends React.Component {
  state = {
    value: moment('2019-03-16'),
    selectedValue: moment('2019-03-16'),
    val: 'its Me',
    fields: {}
  };
  showModal = () => {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  };
  onSelect = value => {
    this.setState({
      value,
      selectedValue: value,
    });
  };
  onClick = val => {
    this.setState({
      val,
    });
  };
  onPanelChange = value => {
    this.setState({ value });
  };
  handleDateChange = (dateName, dateValue) => {
    this.setState({
      fields: {
        ...this.fields,
        [dateName]: dateValue
      }
    })
  };
  
  render() {
    const { value, val, selectedValue } = this.state;
    const { RangePicker } = DatePicker;
    moment.updateLocale('en', {
      weekdaysMin : ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
    });
    const columns = [
      {
        title: 'Sun',
        dataIndex: 'Sun',
        width:'12%'
      },
      {
        title: 'Mon',
        dataIndex: 'Mon',
        width:'12%'
      },
      {
        title: 'Tue',
        dataIndex: 'Tue',
        width:'12%'
      },
      {
        title: 'Wed',
        dataIndex: 'Wed',
        width:'12%'
      },
      {
        title: 'Thu',
        dataIndex: 'Thu',
        width:'12%'
      },
      {
        title: 'Fri',
        dataIndex: 'Fri',
        width:'12%'
      },
      {
        title: 'Sat',
        dataIndex: 'Sat',
        width:'12%'
      },
    ];
    
const data = [
  {
    key: '1',
    Sun: <Checkbox>Available</Checkbox>,
    Mon: <Checkbox>Available</Checkbox>,
    Tue: <Checkbox>Available</Checkbox>,
    Wed: <Checkbox>Available</Checkbox>,
    Thu: <Checkbox>Available</Checkbox>,
    Fri: <Checkbox>Available</Checkbox>,
    Sat: <Checkbox>Available</Checkbox>,
  },
  {
    key: '2',
    Sun: <Checkbox style={{color:'red'}}>UnAvailable</Checkbox>,
    Mon: <Checkbox style={{color:'red'}}>UnAvailable</Checkbox>,
    Tue: <Checkbox style={{color:'red'}}>UnAvailable</Checkbox>,
    Wed: <Checkbox style={{color:'red'}}>Unailable</Checkbox>,
    Thu: <Checkbox style={{color:'red'}}>UnAvailable</Checkbox>,
    Fri: <Checkbox style={{color:'red'}}>UnAvailable</Checkbox>,
    Sat: <Checkbox style={{color:'red'}}>UnAvailable</Checkbox>,
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
  },
];
    return (
      <div>
        {/* <Alert
          message={`You selected date: ${selectedValue && selectedValue.format('YYYY-MM-DD')}`}
        /> */}
        {/* <Calendar  value={this.fields["value"]}
    onChange={(value) => this.handleDateChange("value", value)}/> */}
        <div>
          <RangePicker />
        </div>
        
        <div>
        <Button  type="primary" style={{marginTop: '10px'}} onClick={this.showModal}>Appointment Shedule</Button>
        </div>
        <div style={{display:'none'}}id="myDIV">
        {/* <DatePicker updateLocale={locale} />
        <DatePicker defaultValue={moment('2015-01-01', 'YYYY-MM-DD')} />; */}
        <Table columns={columns} dataSource={data} size="middle" />
        <Button>Submit</Button>
        <Button>Cancel</Button>
</div>
      </div>
    );
  }
}
export default SamplePage;