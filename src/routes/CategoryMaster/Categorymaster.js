import React, { Component } from "react";
import { Button, Divider, Modal, Card, Table, Form, Input } from "antd";
import AddCategory from './AddCategory';
const FormItem = Form.Item;
const confirm = Modal.confirm;
const data = [{
    key: '1',
    category: 'lawyer',
    subcategory: 'civil lawyer',
}, {
    key: '2',
    category: 'lawyer',
    subcategory: 'civil lawyer',
}, {
    key: '3',
    category: 'lawyer',
    subcategory: 'civil lawyer',
}, {
    key: '4',
    category: 'lawyer',
    subcategory: 'civil lawyer',
}];

class CategoryMaster extends React.Component {
    state = {
        filteredInfo: null,
        sortedInfo: null,
        visible: false,
        open: false
    };

    showDeleteConfirm = () => {
        confirm({
            title: 'Are you sure delete this task?',
            content: 'Some descriptions',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk() {
                console.log('OK');
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }
    opencategory = () => {
        this.setState({
            open: true,
        })
    };
    showModal = () => {
        this.setState({
            visible: true,
        });
    };
    handleOk = (e) => {
        console.log(e);
        this.setState({
            visible: false,
            open:false
        });
    };
    handleCancel = (e) => {
        console.log(e);
        this.setState({
            visible: false,
            open:false
        });
    };
    handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter);
        this.setState({
            filteredInfo: filters,
            sortedInfo: sorter,
        });
    };
    clearFilters = () => {
        this.setState({ filteredInfo: null });
    };
    clearAll = () => {
        this.setState({
            filteredInfo: null,
            sortedInfo: null,
        });
    };
    setAgeSort = () => {
        this.setState({
            sortedInfo: {
                order: 'descend',
                columnKey: 'age',
            },
        });
    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }
    render() {
        // const {getFieldDecorator} = this.props.form;
        let { sortedInfo, filteredInfo } = this.state;
        sortedInfo = sortedInfo || {};
        filteredInfo = filteredInfo || {};
        const columns = [
            {
                title: 'Category',
                dataIndex: 'category',
                key: 'category',
            },
            {
                title: 'Sub-Category',
                dataIndex: 'subcategory',
                key: 'subcategory',
            },
            {
                title: 'Action',
                key: '',
                render: () =>
                    <span>
                        <span style={{ cursor: 'pointer' }} onClick={this.showModal} className="icon icon-edit"></span>
                        <Divider type="vertical" />
                        <span style={{ cursor: 'pointer' }} onClick={this.showDeleteConfirm} className="icon icon-close-circle"></span>
                    </span>
            }];

        return (
            <div>
                <Card title="Category Master">
                    <div className="table-operations">
                        {/* <Button onClick={this.setAgeSort}>Sort age</Button>
                        <Button onClick={this.clearFilters}>Clear filters</Button>
                        <Button onClick={this.clearAll}>Clear filters and sorters</Button> */}
                        <Button type="primary" onClick={this.opencategory}>Add Category</Button>
                    </div>
                    <Table className="gx-table-responsive" columns={columns} dataSource={data} onChange={this.handleChange} />
                </Card>
                <Modal
                    title=" Edit Category Master"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <Form onSubmit={this.handleSubmit}>
                        <FormItem
                            label="Category"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                        <FormItem
                            label="Sub-Category"
                            labelCol={{ xs: 24, sm: 8 }}
                            wrapperCol={{ xs: 24, sm: 16 }}
                        >
                            <Input />
                        </FormItem>
                       
                        {/* <FormItem
              wrapperCol={{ xs: 24, sm: { span: 12, offset: 5 } }}
            >
              <Button type="primary" htmlType="submit">
                Submit
            </Button>
            </FormItem> */}
                    </Form>
                </Modal>
                <Modal
                    title="Create Category"
                    visible={this.state.open}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <AddCategory/>
                </Modal>
            </div>
        );
    }
}

export default CategoryMaster;
